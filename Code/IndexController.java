package be.helha.aemt.control;

import java.io.Serializable;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.Lob;

@Named
@SessionScoped
public class IndexController implements Serializable {
    private String srcPage="home.xhtml";

    public void doGoList() {
        this.srcPage="activity/activity_list.xhtml";
    }
    
    public void doGoAboutUs() {
    	this.srcPage="about_us.xhtml";
    }

    public void doGoLogOut() {
    	this.srcPage="logout_page.xhtml";
    }

    public void doGoHome() {
        this.srcPage="home.xhtml";
    }

    public void doGoAdminHome() {
        this.srcPage="admin/admin_home.xhtml";
    }

    public void doGoLogIn() {
        this.srcPage="login.xhtml";
    }

    public void doGoUserProfile() {
    	this.srcPage="user/profile.xhtml";
    }

    public void doGoGlobalAgenda()
    {
        this.srcPage = "agenda_global.xhtml";
    }

    public String getSrcPage() {
        return srcPage;
    }

    public void setSrcPage(String srcPage) {
        this.srcPage = srcPage;
    }

    //@Lob
}
